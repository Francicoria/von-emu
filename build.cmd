@echo off

set CC=gcc
set CFLAGS=-Wall -Wextra -Wpedantic -Wno-type-limits -ggdb

::%CC% %CFLAGS% -o emu emu.c
%CC% %CFLAGS% -o new_lang new_lang.c tokenizer.c
