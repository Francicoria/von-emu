#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#define report_fmt(level, s, ...) fprintf(stderr, #level ": " s "\n", __VA_ARGS__)
#define report(level, s) fprintf(stderr, #level ": " s "\n")

#define ARRAY_LEN(xs) (sizeof(xs) / sizeof(xs[0]))

#define REGISTERS_COUNT 3

#define REGISTER_CMP_FLAGS 2
// .  .  .  .  .  .  .  .
//                >  <  ==
#define CMP_FLAGS_EQ (1 << 0)
#define CMP_FLAGS_LT (1 << 1)
#define CMP_FLAGS_GT (1 << 2)
#define CHECK_EQ(x) ((x & CMP_FLAGS_EQ) >> 0)
#define CHECK_LT(x) ((x & CMP_FLAGS_LT) >> 1)
#define CHECK_GT(x) ((x & CMP_FLAGS_GT) >> 2)
#define CHECK_LTEQ(x) (CHECK_LT(x) | CHECK_EQ(x))
#define CHECK_GTEQ(x) (CHECK_GT(x) | CHECK_EQ(x))

// :instructions
typedef enum {
	EXIT  = 0x0,

	STORE,
	LOAD,
	CMP,

	JMP,
	JMPE,
	JMPL,
	JMPG,
	JMPLE,
	JMPGE,

	INCR,
	DECR,
	MUL,
	DIV,

	COUNT_INST,
} Inst;

typedef uint32_t Register;
typedef uint32_t Mem_Cell;

uint32_t expect_arg(uint32_t *program, size_t program_len, size_t *offset) {
	(*offset)++;
	if (*offset >= program_len) {
		report(ERROR, "Expected an argument but got end of instructions instead");
		exit(1);
	}
	return program[*offset];
}

void print_registers(Register *regs, size_t regs_len) {
	printf("Registers: {\n  ");
	for (size_t i = 0; i < regs_len; ++i) {
		printf("r%c = ", (char)i + 'A');
		if (i == REGISTER_CMP_FLAGS) {
			printf("b%u%u%u", CHECK_GT(regs[i]), CHECK_LT(regs[i]), CHECK_EQ(regs[i]));
		} else {
			printf("%3u", regs[i]);
		}
		printf(", ");
	}
	printf("\n}\n");
}

void print_memory(Mem_Cell *mem, size_t mem_len) {
	printf("Memory: [\n  ");
	for (size_t i = 0; i < mem_len; ++i) {
		bool trailing_zeros = false;
		for (size_t j = i; j < mem_len; ++j) {
			trailing_zeros = !mem[j];
			if (mem[j]) break;
		}
		if (trailing_zeros) {
			printf("0 ... <trailing zeros>");
			break;
		}
		printf("%u ", mem[i]);
	}
	printf("\n]\n");
}

bool simulate_program(uint32_t *program, size_t program_len, Register *regs, size_t regs_len, Mem_Cell *mem, size_t mem_len) {
	// if an instruction doesn't want the program counter to increment (because it increments it itself) then it sets this to false;
	// in the next iterations this will be reset to true.
	bool incr = true;

	for (size_t i = 0; i < program_len; incr ? i += 1 : (void)i) {
#define expect_arg(type, name, i) type name = expect_arg(program, program_len, &i)

#define expect_mem_addr(name, i) \
		expect_arg(Mem_Cell, name, i); \
		do { \
			if (name >= mem_len) { \
				report_fmt(ERROR, "Memory address provided (%u) doesn't exist", name); \
				return false; \
			} \
		} while (0)

#define expect_reg(name, i) \
		expect_arg(Register, name, i); \
		do { \
			if (name >= regs_len) { \
				report_fmt(ERROR, "Register provided (%u) doesn't exist", name); \
				return false; \
			} \
		} while (0)

#define expect_iaddr(name, i) \
		expect_arg(uint32_t, name, i); \
		do { \
			if (name >= program_len) { \
				report_fmt(ERROR, "Instruction address provided (%u) doesn't exist", name); \
				return false; \
			} \
		} while (0)

		incr = true;

		uint32_t cursor = program[i];
		static_assert(COUNT_INST == 14, "An instruction was added but not implemented");
		switch ((Inst)cursor) {
			case EXIT: {
				printf("EXIT\n");
				goto exit_loop;
			} break;
			case STORE: {
				expect_mem_addr(mdest, i);
				expect_arg(uint32_t, value, i);

				printf("STORE %u %u\n", mdest, value);

				mem[mdest] = value;
			} break;
			case LOAD: {
				expect_reg(rdest, i);
				expect_mem_addr(msrc, i);

				printf("LOAD %u %u\n", rdest, msrc);

				regs[rdest] = mem[msrc];
			} break;
			case CMP: {
				expect_reg(reg1, i);
				expect_reg(reg2, i);

				printf("CMP %u %u\n", reg1, reg2);

				uint32_t r1 = regs[reg1];
				uint32_t r2 = regs[reg2];

				uint32_t res = 0;
				res = (r1 == r2) << 0 |
				      (r1  < r2) << 1 |
				      (r1  > r2) << 2;

				regs[REGISTER_CMP_FLAGS] = res;
			} break;
			case JMP: {
				expect_iaddr(iaddr, i);

				printf("JMP %u\n", iaddr);

				i = iaddr;
				incr = false;
			} break;
			case JMPE: {
				expect_iaddr(iaddr, i);

				printf("JMPE %u\n", iaddr);

				if (CHECK_EQ(regs[REGISTER_CMP_FLAGS])) {
					i = iaddr;
					incr = false;
				}
			} break;
			case JMPL: {
				expect_iaddr(iaddr, i);

				printf("JMPL %u\n", iaddr);

				if (CHECK_LT(regs[REGISTER_CMP_FLAGS])) {
					i = iaddr;
					incr = false;
				}
			} break;
			case JMPG: {
				expect_iaddr(iaddr, i);

				printf("JMPG %u\n", iaddr);

				if (CHECK_GT(regs[REGISTER_CMP_FLAGS])) {
					i = iaddr;
					incr = false;
				}
			} break;
			case JMPLE: {
				expect_iaddr(iaddr, i);

				printf("JMPLE %u\n", iaddr);

				if (CHECK_LTEQ(regs[REGISTER_CMP_FLAGS])) {
					i = iaddr;
					incr = false;
				}
			} break;
			case JMPGE: {
				expect_iaddr(iaddr, i);

				printf("JMPGE %u\n", iaddr);

				if (CHECK_GTEQ(regs[REGISTER_CMP_FLAGS])) {
					i = iaddr;
					incr = false;
				}
			} break;
			case INCR: {
				expect_reg(reg, i);
				expect_arg(uint32_t, value, i);

				printf("INCR %u %u\n", reg, value);

				if (regs[reg] + value > UINT32_MAX) {
					report_fmt(ERROR, "Unsigned overflow incrementing register %u (which holds %u) by %u", reg, regs[reg], value);
					return false;
				}
				regs[reg] += value;
			} break;
			case DECR: {
				expect_reg(reg, i);
				expect_arg(uint32_t, value, i);

				printf("DECR %u %u\n", reg, value);

				if (regs[reg] - value < 0) {
					report_fmt(ERROR, "Unsigned underflow decrementing register %u (which holds %u) by %u", reg, regs[reg], value);
					return false;
				}
				regs[reg] -= value;
			} break;
			case MUL: {
				expect_reg(reg, i);
				expect_arg(uint32_t, value, i);

				printf("MUL %u %u\n", reg, value);

				if (regs[reg] * value > UINT32_MAX) {
					report_fmt(ERROR, "Unsigned overflow multiplying register %u (which holds %u) by %u", reg, regs[reg], value);
					return false;
				}
				regs[reg] *= value;
			} break;
			case DIV: {
				expect_reg(reg, i);
				expect_arg(uint32_t, value, i);

				printf("DIV %u %u\n", reg, value);

				if (value == 0) {
					report_fmt(ERROR, "Dividing register %u by 0", reg);
					return false;
				}
				regs[reg] /= value;
			} break;

			default: {
				report_fmt(ERROR, "Illegal instruction %u", cursor);
				return false;
			} break;
		}
	}
	report(WARNING, "Program finished but EXIT wasn't encountered");

exit_loop:

	return true;
}

int main(void) 	{
	Register regs[REGISTERS_COUNT] = {0};
	Mem_Cell mem[256] = {0};

	print_registers(regs, ARRAY_LEN(regs));
	print_memory(mem, ARRAY_LEN(mem));

	// :program
	uint32_t program[] = {
		STORE, 0, 512,
		LOAD, 0, 0,
		DIV, 0, 2,

		EXIT,
	};

	if (!simulate_program(program, ARRAY_LEN(program), regs, ARRAY_LEN(regs), mem, ARRAY_LEN(mem))) {
		return 1;
	}

	print_registers(regs, ARRAY_LEN(regs));
	print_memory(mem, ARRAY_LEN(mem));

	return 0;
}
