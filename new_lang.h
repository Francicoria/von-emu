#ifndef NEW_LANG_H
#define NEW_LANG_H

#include <stdint.h>
#include <stddef.h>

#include "tokenizer.h"

#define dyn_append(dyn_arr, new_element) do { \
	if ((dyn_arr)->len + 1 >= (dyn_arr)->cap) { \
		(dyn_arr)->cap = 2 * (dyn_arr)->cap + 1; \
		(dyn_arr)->elems = realloc((dyn_arr)->elems, sizeof(*(dyn_arr)->elems) * (dyn_arr)->cap); \
		if (!(dyn_arr)->elems) { \
			fprintf(stderr, "Couldn't realloc!\n"); \
			abort(); \
		} \
	} \
	(dyn_arr)->elems[(dyn_arr)->len] = new_element; \
	(dyn_arr)->len += 1; \
} while (0)

#define dyn_push dyn_append
// uuuuh don't look don't look don't look don't look don't look don't look don't look don't look
#define dyn_pop(dyn_arr) \
	( \
	  ((dyn_arr)->len - 1 < 0 ? fprintf(stderr, "reached length 0, can't pop!"), abort(), NULL : NULL), \
	  (dyn_arr)->elems[--(dyn_arr)->len] \
	  )


// @todo: implement source code location reporting
typedef enum {
	REPORT_INTERNAL_ERROR = 0,
	REPORT_ERROR,
	REPORT_INFO,
} ReportLevel;
static void report(ReportLevel level, const char *fmt, ...);
#define UNREACHABLE() do { \
	report(REPORT_INTERNAL_ERROR, "unreachable, line: %d", __LINE__); \
	abort(); \
} while (0)

static uint8_t *bump_alloc(size_t n);

enum BinaryOperatorType {
	BOT_ADDITION = 0,
	BOT_SUBTRACTION,
	BOT_MULTIPLICATION,
};
const char *get_bot_representation(enum BinaryOperatorType bot);
enum BinaryOperatorType bot_from_tt(enum TokenType tt);

enum ExprType {
	ET_NUMBER = 0,
	ET_IDENTIFIER,
	ET_BINARY_OP,
	ET_FUNCTION_CALL,
	ET_BLOCK,
};

typedef struct Expr Expr;

typedef struct {
	StringV identifier;
	Expr *expr;
} Binding;

typedef struct {
	Binding *elems;
	size_t len;
	size_t cap;
} Bindings;

typedef struct {
	Bindings bindings;
	Expr *return_expr;
} Block;

typedef struct Expr {
	enum ExprType type;
	union {
		int number;
		StringV identifier;
		struct BinaryOperator {
			enum BinaryOperatorType operator;
			struct Expr *lexpr;
			struct Expr *rexpr;
		} bop;
		struct FunctionCall {
			StringV identifier;
			struct Expr *argument;
		} fc;
		Block block;
	};
} Expr;
static void print_expr(Expr *e);

static Expr *alloc_expr(void);
static Expr *alloc_expr_number(int number);
static Expr *alloc_expr_identifier(StringV identifier);
static Expr *alloc_expr_binary_op(enum BinaryOperatorType operator, Expr *lexpr, Expr *rexpr);
static Expr *alloc_expr_function_call(StringV identifier, Expr *argument);
static Expr *alloc_expr_block(Block block);

typedef struct {
	Token *elems;
	size_t len;
	size_t cap;
} Tokens;

static void associate_parenthesis(Tokens tokens);

typedef struct {
	enum TokenType op;
	size_t index;
} Operator_Info;

// lower precedence first, higher preced. last
enum TokenType op_precedence[] = {
	TT_PLUS,
	TT_MINUS,
	TT_ASTERISK,
};

static size_t get_precedence(enum TokenType op);
static Expr *parse_expr(Tokens tokens);

static Binding parse_binding(Tokens tokens);

static Block parse_block(Tokens tokens);

typedef struct {
	StringV *elems;
	size_t len;
	size_t cap;
} Identifiers;

static bool compile_expr(Expr *e, FILE *output, Identifiers *identifiers);
static bool compile_binding(Binding bind, FILE *output, Identifiers *identifiers);
static void compile(Block program_block, const char *output_file_path);

static void validate_identifiers(Block block, Identifiers *scoped_identifiers);

static void print_binding(Binding binding);
static void print_block(Block block);

#endif // NEW_LANG_H
