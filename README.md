# von-emu

Emulator for an imaginary machine which follows the [Von Neumann architecture](https://en.wikipedia.org/wiki/Von_Neumann_architecture).

## Compile and run
```console
$ ./build.sh
$ ./emu
```
