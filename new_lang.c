#include <assert.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tokenizer.h"
#include "new_lang.h"

static void report(ReportLevel level, const char *fmt, ...) {
	switch (level) {
	case REPORT_INTERNAL_ERROR: {
		printf("(INTERNAL_ERROR) ");
	} break;
	case REPORT_ERROR: {
		printf("(ERROR) ");
	} break;
	case REPORT_INFO: {
		printf("(INFO) ");
	} break;
	}
	va_list args;
	va_start(args, fmt);
	vprintf(fmt, args);
	va_end(args);

	printf("\n");
}

#define BUMP_BUFFER_CAP 1024
struct {
	uint8_t bytes[BUMP_BUFFER_CAP];
	size_t cursor;
} bump_buf;
static uint8_t *bump_alloc(size_t n) {
	if (bump_buf.cursor + n >= BUMP_BUFFER_CAP) {
		report(REPORT_INTERNAL_ERROR, "bump allocator cap exceeded, can't allocate");
		abort();
	}

	uint8_t *ptr = &bump_buf.bytes[bump_buf.cursor];
	bump_buf.cursor += n;
	return ptr;
}

const char *get_bot_representation(enum BinaryOperatorType bot) {
	switch (bot) {
	case BOT_ADDITION: return "+";
	case BOT_SUBTRACTION: return "-";
	case BOT_MULTIPLICATION: return "*";
	}
	UNREACHABLE();
}

enum BinaryOperatorType bot_from_tt(enum TokenType tt) {
	switch (tt) {
	case TT_PLUS: return BOT_ADDITION;
	case TT_MINUS: return BOT_SUBTRACTION;
	case TT_ASTERISK: return BOT_MULTIPLICATION;
	default: {
		report(REPORT_INTERNAL_ERROR, "trying to get binary operator from %s", get_tt_name(tt));
		abort();
	}
	}
}

static void print_expr(Expr *e) {
	switch (e->type) {
	case ET_NUMBER: {
		printf("number<%d>", e->number);
	} break;

	case ET_IDENTIFIER: {
		printf("ident<" StringV_fmt ">", StringV_arg(e->identifier));
	} break;

	case ET_BINARY_OP: {
		printf("(");
		print_expr(e->bop.lexpr);
		printf(" %s ", get_bot_representation(e->bop.operator));
		print_expr(e->bop.rexpr);
		printf(")");
	} break;

	case ET_FUNCTION_CALL: {
		printf(StringV_fmt "(", StringV_arg(e->fc.identifier));
		print_expr(e->fc.argument);
		printf(")");
	} break;

	case ET_BLOCK: {
		printf("{ ");
		for (size_t i = 0; i < e->block.bindings.len; ++i) {
			printf(StringV_fmt " = ", StringV_arg(e->block.bindings.elems[i].identifier));
			print_expr(e->block.bindings.elems[i].expr);
			printf(", ");
		}
		print_expr(e->block.return_expr);
		printf(" }");

	} break;
	}
}

static Expr *alloc_expr(void) {
	return (Expr *)bump_alloc(sizeof(Expr));
}

static Expr *alloc_expr_number(int number) {
	Expr *e = alloc_expr();
	e->type = ET_NUMBER;
	e->number = number;
	return e;
}
static Expr *alloc_expr_identifier(StringV identifier) {
	Expr *e = alloc_expr();
	e->type = ET_IDENTIFIER;
	e->identifier = identifier;
	return e;
}

static Expr *alloc_expr_binary_op(enum BinaryOperatorType operator, Expr *lexpr, Expr *rexpr) {
	Expr *e = alloc_expr();
	e->type = ET_BINARY_OP;
	e->bop.operator = operator;
	e->bop.lexpr = lexpr;
	e->bop.rexpr = rexpr;
	return e;
}

static Expr *alloc_expr_function_call(StringV identifier, Expr *argument) {
	Expr *e = alloc_expr();
	e->type = ET_FUNCTION_CALL;
	e->fc.identifier = identifier;
	e->fc.argument = argument;
	return e;
}

static Expr *alloc_expr_block(Block block) {
	Expr *e = alloc_expr();
	e->type = ET_BLOCK;
	e->block = block;

	return e;
}

typedef struct {
	size_t *elems;
	size_t len;
	size_t cap;
} Parenthesis_Index_Stack;

static void associate_parenthesis(Tokens tokens) {
	bool ok = true;
	Parenthesis_Index_Stack stack = {0};

	for (size_t i = 0; i < tokens.len; ++i) {
		switch (tokens.elems[i].type) {
		case TT_OPENING_PAREN:
		case TT_OPENING_BRACE: {
			dyn_push(&stack, i);
		} break;

		case TT_CLOSING_PAREN:
		case TT_CLOSING_BRACE: {
			size_t opening_index = dyn_pop(&stack);
			enum TokenType opening_type = tokens.elems[opening_index].type;

			// this depends on the fact that in the TokenType enumeration we store
			// the same type of parenthesis next to eachother, so
			// TT_OPENING_<type of parenthesis> + 1 == TT_CLOSING_<type of parenthesis>
			if (opening_type + 1 != tokens.elems[i].type) {
				report(REPORT_ERROR, "parenthesis mismatch, found %s, expected %s",
					get_tt_name(tokens.elems[i].type),
					get_tt_name(opening_type + 1));
				ok = false;
				goto cleanup;
			}

			int offset = i - opening_index;
			tokens.elems[opening_index].value.matching_paren_relative_index = offset;
			tokens.elems[i].value.matching_paren_relative_index = -offset;
		} break;

		default: break;
		}
	}

	if (stack.len > 0) {
		report(REPORT_ERROR, "unmatched opening parenthesis");
		ok = false;
		goto cleanup;
	}

cleanup:
	if (stack.elems) free(stack.elems);
	if (!ok) exit(1);
}

static size_t get_precedence(enum TokenType op) {
	for (size_t i = 0; i < ARRAY_LEN(op_precedence); ++i) {
		if (op == op_precedence[i]) return i;
	}
	UNREACHABLE();
}

typedef struct {
	Operator_Info *elems;
	size_t len;
	size_t cap;
} Encountered_Operators;

static Expr *parse_expr_(Tokens tokens) {
	if (tokens.len == 0) {
		report(REPORT_ERROR, "expected 1 token, only got %zu", tokens.len);
		goto cleanup_and_exit;
	}

	if (tokens.len >= 2) {
		if (tokens.elems[0].type == TT_OPENING_PAREN && tokens.elems[tokens.len-1].type == TT_CLOSING_PAREN) {
			tokens.elems += 1;
			tokens.len -= 2;

			if (tokens.len == 0) {
				report(REPORT_ERROR, "nothing inside parenthesis");
				goto cleanup_and_exit;
			}
		} else if (tokens.elems[0].type == TT_OPENING_BRACE && tokens.elems[tokens.len-1].type == TT_CLOSING_BRACE) {
			Tokens to_parse = {
				.elems = tokens.elems + 1,
				.len = tokens.len - 2,
			};
			return alloc_expr_block(parse_block(to_parse));
		}
	}

	if (tokens.len == 1) {
		Token single = tokens.elems[0];
		switch (single.type) {
		case TT_NUMBER: return alloc_expr_number(single.value.number);
		case TT_IDENTIFIER: return alloc_expr_identifier(single.value.identifier);
		default: {
			report(REPORT_ERROR, "expected TT_NUMBER or TT_IDENTIFIER, got %s", get_tt_name(single.type));
			goto cleanup_and_exit;
		}
		}
	}

	Encountered_Operators encountered_ops = {0};
	for (size_t i = 0; i < tokens.len; ++i) {
		Token token = tokens.elems[i];
		switch (token.type) {
		case TT_IDENTIFIER:
		case TT_NUMBER: break;

		case TT_PLUS:
		case TT_MINUS:
		case TT_ASTERISK: {
			Operator_Info oi = { token.type, i };
			dyn_append(&encountered_ops, oi);
		} break;

		case TT_OPENING_PAREN:
		case TT_OPENING_BRACE: {
			if (token.value.matching_paren_relative_index == 0) UNREACHABLE();
			i += token.value.matching_paren_relative_index;
		} break;

		case TT_CLOSING_PAREN:
		case TT_CLOSING_BRACE: UNREACHABLE();

		default: {
			report(REPORT_ERROR, "encountered unknown token: %s", get_tt_name(token.type));
			goto cleanup_and_exit;
		}
		}
	}

	if (encountered_ops.len == 0) {
		if (tokens.len >= 3 &&
		    tokens.elems[0].type == TT_IDENTIFIER &&
		    tokens.elems[1].type == TT_OPENING_PAREN &&
		    (size_t)tokens.elems[1].value.matching_paren_relative_index + 1 == tokens.len - 1) {
			// @todo: multiple/no argument function calls here
			Tokens to_parse = {
				.elems = tokens.elems + 2,
				.len = tokens.len - 3
			};
			return alloc_expr_function_call(tokens.elems[0].value.identifier, parse_expr_(to_parse));
		} else {
			report(REPORT_INTERNAL_ERROR, "while searching in the tokens for operators found none");
			abort();
		}
	}

	Operator_Info least_precedence_op = encountered_ops.elems[0];
	for (size_t i = 0; i < encountered_ops.len; ++i) {
		if (get_precedence(least_precedence_op.op) > get_precedence(encountered_ops.elems[i].op))
			least_precedence_op = encountered_ops.elems[i];
	}
	free(encountered_ops.elems);

	switch (least_precedence_op.op) {
	case TT_PLUS:
	case TT_MINUS:
	case TT_ASTERISK: {
		Tokens lexpr = {
			.elems = tokens.elems,
			.len = least_precedence_op.index
		};
		Tokens rexpr = {
			.elems = tokens.elems + least_precedence_op.index + 1,
			.len = tokens.len - least_precedence_op.index - 1
		};
		return alloc_expr_binary_op(bot_from_tt(least_precedence_op.op), parse_expr_(lexpr), parse_expr_(rexpr));
	}
	// @todo: implement unary operators here
	default: UNREACHABLE();
	}

cleanup_and_exit:
	if (encountered_ops.elems) free(encountered_ops.elems);
	exit(1);
}

static Expr *parse_expr(Tokens tokens) {
	associate_parenthesis(tokens);
	return parse_expr_(tokens);
}

static Binding parse_binding(Tokens tokens) {
	if (tokens.len < 2) {
		report(REPORT_ERROR, "expected 2 tokens, only got %zu", tokens.len);
		exit(1);
	}
	if (tokens.elems[0].type != TT_IDENTIFIER) {
		report(REPORT_ERROR, "expected TT_IDENTIFIER, got %s", get_tt_name(tokens.elems[0].type));
		exit(1);
	}
	if (tokens.elems[1].type != TT_EQUAL) {
		report(REPORT_ERROR, "expected TT_EQUAL, got %s", get_tt_name(tokens.elems[1].type));
		exit(1);
	}
	Tokens to_parse = {
		.elems = tokens.elems + 2,
		.len = tokens.len - 2,
	};

	Expr *expr = parse_expr(to_parse);

	return (Binding) { .identifier = tokens.elems[0].value.identifier, .expr = expr };
}

static Block parse_block(Tokens tokens) {
	associate_parenthesis(tokens);

	Block block = {0};
	size_t current_binding_start_index = 0;
	for (size_t i = 0; i < tokens.len; ++i) {
		Token t = tokens.elems[i];

		switch (t.type) {
		case TT_OPENING_BRACE: {
			if (t.value.matching_paren_relative_index == 0) UNREACHABLE();
			i += t.value.matching_paren_relative_index;
		} break;

		case TT_COMMA: {
			Tokens to_parse = {
				.elems = tokens.elems + current_binding_start_index,
				.len = i - current_binding_start_index
			};
			Binding bind = parse_binding(to_parse);
			dyn_append(&block.bindings, bind);
			current_binding_start_index = i + 1;
		} break;

		default: break;
		}
	}
	Tokens to_parse = {
		.elems = tokens.elems + current_binding_start_index,
		.len = tokens.len - current_binding_start_index
	};
	block.return_expr = parse_expr(to_parse);

	return block;
}

const char *fasm_starting_blob =
	"format ELF64 executable 3\n"
	"\n"
	"stdin_fd equ 0\n"
	"stdout_fd equ 1\n"
	"stderr_fd equ 2\n"
	"\n"
	"; https://syscalls64.paolostivanin.com\n"
	"sys_write equ 0x01\n"
	"sys_exit equ 0x3c\n"
	"\n"
	"segment readable executable\n"
	"entry _start\n"
	"_start:\n";

const char *fasm_ending_blob =
	"mov rax, sys_exit\n"
	"pop rdi\n"
	"syscall\n"
	"\n"
	"; prints first element on the stack and pops it\n"
	"putd:\n"
	"PUTD_BUF_CAP equ 24\n"
	"	pop rdi\n"
	"	pop rax\n"
	"	push rdi\n"
	"	mov rbx, putd_buf\n"
	"\n"
	"	mov rcx, 10\n"
	"\n"
	"	mov r8, 1\n"
	"\n"
	"	.loop:\n"
	"		dec rbx\n"
	"		inc r8\n"
	"\n"
	"		xor rdx, rdx\n"
	"		div rcx\n"
	"		add dl, '0'\n"
	"		mov byte [rbx], dl\n"
	"\n"
	"		test rax, rax\n"
	"		jnz .loop\n"
	"\n"
	"	mov rax, sys_write\n"
	"	mov rdi, stdout_fd\n"
	"	mov rsi, rbx\n"
	"	mov rdx, r8\n"
	"	syscall\n"
	"\n"
	"	ret\n"
	"\n"
	"segment readable writeable\n"
	"\n"
	"rb PUTD_BUF_CAP\n"
	"putd_buf: db 0xa\n";

// @todo: check for overflows
static bool compile_expr(Expr *e, FILE *output, Identifiers *identifiers) {
	switch (e->type) {
	case ET_NUMBER: {
		if (fprintf(output, "; NUMBER %d\n", e->number) < 0) return false;
		if (fprintf(output, "push qword %d\n", e->number) < 0) return false;
	} break;

	case ET_IDENTIFIER: {
		if (fprintf(output, "; IDENTIFIER " StringV_fmt "\n", StringV_arg(e->identifier)) < 0) return false;
		if (fprintf(output, "push qword [" StringV_fmt "]\n", StringV_arg(e->identifier)) < 0) return false;
	} break;

	case ET_BINARY_OP: {
		compile_expr(e->bop.lexpr, output, identifiers);
		compile_expr(e->bop.rexpr, output, identifiers);

		if (fprintf(output, "; BINOP %s\n", get_bot_representation(e->bop.operator)) < 0) return false;
		if (fprintf(output, "pop rbx\n") < 0) return false;
		if (fprintf(output, "pop rax\n") < 0) return false;

		switch (e->bop.operator) {
		case BOT_ADDITION: {
			if (fprintf(output, "add rax, rbx\n") < 0) return false;
		} break;
		case BOT_SUBTRACTION: {
			if (fprintf(output, "sub rax, rbx\n") < 0) return false;
		} break;
		case BOT_MULTIPLICATION: {
			if (fprintf(output, "mul rbx\n") < 0) return false;
		} break;
		}

		if (fprintf(output, "push rax\n") < 0) return false;
	} break;

	case ET_FUNCTION_CALL: {
		assert(StringV_equal(e->fc.identifier, StringV_literal("putd")));
		compile_expr(e->fc.argument, output, identifiers);
		if (fprintf(output, "; FUNCALL " StringV_fmt "\n", StringV_arg(e->fc.identifier)) < 0) return false;
		if (fprintf(output, "call " StringV_fmt "\n", StringV_arg(e->fc.identifier)) < 0) return false;
	} break;

	case ET_BLOCK: {
		for (size_t i = 0; i < e->block.bindings.len; ++i)
			compile_binding(e->block.bindings.elems[i], output, identifiers);
		compile_expr(e->block.return_expr, output, identifiers);
	} break;
	}

	return true;
}

static bool compile_binding(Binding bind, FILE *output, Identifiers *identifiers) {
	if (!compile_expr(bind.expr, output, identifiers)) return false;
	if (fprintf(output, "pop rax\n") < 0) return false;
	if (fprintf(output, "mov [" StringV_fmt "], rax\n", StringV_arg(bind.identifier)) < 0) return false;
	dyn_append(identifiers, bind.identifier);

	return true;
}

static void compile(Block program_block, const char *output_file_path) {
	bool ok = true;

	FILE *output = fopen(output_file_path, "w");
	if (!output) {
		report(REPORT_ERROR, "couldn't open %s for writing: %s", output_file_path, strerror(errno));

		ok = false;
		goto cleanup;
	}

	if (fputs(fasm_starting_blob, output) < 0) {
		report(REPORT_ERROR, "couldn't write to %s: %s", output_file_path, strerror(errno));

		ok = false;
		goto cleanup;
	}

	Identifiers identifiers = {0};

	for (size_t i = 0; i < program_block.bindings.len; ++i) {
		if (!compile_binding(program_block.bindings.elems[i], output, &identifiers)) {
			report(REPORT_ERROR, "couldn't write to %s: %s", output_file_path, strerror(errno));

			ok = false;
			goto cleanup;
		}
	}

	if (!compile_expr(program_block.return_expr, output, &identifiers)) {
		report(REPORT_ERROR, "couldn't write to %s: %s", output_file_path, strerror(errno));

		ok = false;
		goto cleanup;
	}


	if (fputs(fasm_ending_blob, output) < 0) {
		report(REPORT_ERROR, "couldn't write to %s: %s", output_file_path, strerror(errno));

		ok = false;
		goto cleanup;
	}

	for (size_t i = 0; i < identifiers.len; ++i) {
		if (fprintf(output, StringV_fmt ": dq 0\n", StringV_arg(identifiers.elems[i])) < 0) {
			report(REPORT_ERROR, "couldn't write to %s: %s", output_file_path, strerror(errno));

			ok = false;
			goto cleanup;
		}
	}

cleanup:
	if (output) fclose(output);
	if (identifiers.elems) free(identifiers.elems);
	if (!ok) exit(1);
}

static void validate_identifiers_(Expr *expr, Identifiers *scoped_identifiers) {
	switch (expr->type) {
	case ET_NUMBER: break;

	case ET_IDENTIFIER: {
		for (size_t i = 0; i < scoped_identifiers->len; ++i) {
			if (StringV_equal(expr->identifier, scoped_identifiers->elems[i])) return;
		}

		report(REPORT_ERROR, "'" StringV_fmt "' not found in current scope", StringV_arg(expr->identifier));
		exit(1);
	}

	case ET_BINARY_OP: {
		validate_identifiers_(expr->bop.lexpr, scoped_identifiers);
		validate_identifiers_(expr->bop.rexpr, scoped_identifiers);
	} break;

	case ET_FUNCTION_CALL: {
		validate_identifiers_(expr->fc.argument, scoped_identifiers);
	} break;
	case ET_BLOCK: {
		validate_identifiers(expr->block, scoped_identifiers);
	} break;
	}
}

static void validate_identifiers(Block block, Identifiers *scoped_identifiers) {
	size_t previous_len = scoped_identifiers->len;
	for (size_t i = 0; i < block.bindings.len; ++i) {
		Binding bind = block.bindings.elems[i];
		validate_identifiers_(bind.expr, scoped_identifiers);

		dyn_append(scoped_identifiers, bind.identifier);
	}
	scoped_identifiers->len = previous_len;

	validate_identifiers_(block.return_expr, scoped_identifiers);
}

static void print_binding(Binding binding) {
	printf(StringV_fmt " = ", StringV_arg(binding.identifier));
	print_expr(binding.expr);
	printf(",\n");
}

static void print_block(Block block) {
	printf("+-- block ---\n");
	for (size_t i = 0; i < block.bindings.len; ++i) {
		printf("| ");
		print_binding(block.bindings.elems[i]);
	}

	printf("+-> ");
	print_expr(block.return_expr);
	printf("\n");
}

int main(void) {
	const char code[] =
		"a = { x = 1, 0 },"
		"x";
	Lexer lexer = {
		.content = StringV_literal(code),
	};

	Tokens tokens = {0};

	while (lexer.content.len > 0) {
		Token t = next_token(&lexer);
		dyn_append(&tokens, t);
	}

	Block program_block = parse_block(tokens);

	free(tokens.elems);

	print_block(program_block);

	Identifiers identifiers = {0};
	validate_identifiers(program_block, &identifiers);
	free(identifiers.elems);

	compile(program_block, "out.asm");

	free(program_block.bindings.elems);

	return 0;
}
