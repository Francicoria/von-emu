#ifndef TOKENIZER_H
#define TOKENIZER_H

#include <stdbool.h>
#include <stdint.h>

#define ARRAY_LEN(xs) (sizeof(xs) / sizeof(xs[0]))

typedef struct {
	const char *src;
	size_t len;
} StringV;
#define StringV_literal(str) ((StringV) { .src = (str), .len = ARRAY_LEN((str))-1 })

// for printf use
#define StringV_fmt "%.*s"
#define StringV_arg(str) (int)(str).len, (str).src

bool StringV_equal(StringV s1, StringV s2);
int parse_number(StringV s);

typedef struct {
	StringV content;
} Lexer;

void lexer_trim_left(Lexer *l);
void lexer_trim_right(Lexer *l);

#define TOKENTYPES \
	X(TT_IDENTIFIER) \
	X(TT_NUMBER) \
	X(TT_OPENING_PAREN) \
	X(TT_CLOSING_PAREN) \
	X(TT_OPENING_BRACE) \
	X(TT_CLOSING_BRACE) \
	X(TT_SEMICOLON) \
	X(TT_COLON) \
	X(TT_EQUAL) \
	X(TT_PLUS) \
	X(TT_MINUS) \
	X(TT_ASTERISK) \
	X(TT_COMMA)
enum TokenType {
#define X(n) n,
TOKENTYPES
#undef X
	TT_COUNT,
};
const char *get_tt_name(enum TokenType tt);

typedef struct {
	enum TokenType type;

	union {
		int number;
		StringV identifier;
		int matching_paren_relative_index; // not actually set by the lexer, but set & used in parsing
	} value;
} Token;
Token next_token(Lexer *l);
void print_token(Token t);

#endif // TOKENIZER_H
