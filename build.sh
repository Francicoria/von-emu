#!/bin/sh

set -xe

CC=gcc
CFLAGS="-Wall -Wextra -Wpedantic -Wno-type-limits -ggdb -fsanitize=address"

#${CC} ${CFLAGS} -o emu emu.c
${CC} ${CFLAGS} -o new_lang new_lang.c tokenizer.c
